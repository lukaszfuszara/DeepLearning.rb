class ConvMath
  def padding(volume, padding)
    pad = 0
    while pad < padding
      i = 0
      while i < volume.size
        volume[i].push([0.0] * volume[i].size)
        volume[i].unshift([0.0] * (volume[i].size - 1))
        j = 0
        while j < volume[i].size
          volume[i][j].push(0.0)
          j += 1
        end
        j = 0
        while j < volume[i].size
          volume[i][j].unshift(0.0)
          j += 1
        end
        i += 1
      end
      pad += 1
    end
    volume
  end

  def conv2d(volume, filter, stride)
    filter_size = filter[0].size
    filter = fft_matrix(filter)
    tmp = splice_with_stride(volume, filter_size, stride) { |e| ifft_matrix((Matrix[*fft_matrix(e)].hadamard_product(Matrix[*filter])).to_a) }
    chunk = tmp[0].size * tmp[0][0][0].size
    splice_flatten(tmp, chunk)
  end

  def max_pooling(volume, filter, stride)
    filter_size = filter
    splice_with_stride(volume, filter_size, stride) { |e| e.flatten.max }
  end

  def avg_pooling(volume, filter, stride)
    filter_size = filter
    splice_with_stride(volume, filter_size, stride) { |e| e.flatten.inject(:+) / e.size }
  end

  def normalization(volume)
    out = []
    channel = 0
    while channel < volume.size
      vol = volume[channel].flatten
      vol_size = vol.size
      mean = vol.inject(:+) / vol_size
      var = vol.inject { |sum, x| sum + (x - mean)**2 }
      out[channel] = volume[channel].map { |e| e.map { |f| (f - mean) / Math.sqrt(var + 10**-8) }}
      channel += 1
    end
    out
  end

  def sum_channels(volume)
    array = []
    i = 0
    while i < volume.size
      array[i] = []
      j = 0
      while j < volume[0][0].size
        array[i][j] = []
        k = 0
        while k < volume[0][0].size
          array[i][j][k] = 0.0
          l = 0
          while l < volume[0].size
            array[i][j][k] += volume[i][l][j][k]
            l += 1
          end
          k += 1
        end
        j += 1
      end
      i += 1
    end
    array
  end

  private

  def fft_matrix(matrix)
    array = []
    i = 0
    while i < matrix.size
      array[i] = fft(matrix[i])
      i += 1
    end
    array
  end

  def ifft_matrix(matrix)
    array = []
    i = 0
    while i < matrix.size
      array[i] = ifft(matrix[i])
      i += 1
    end
    array
  end

  def fft(array)
    n = array.size
    return array if n <= 1

    even_vals = array.values_at(* array.each_index.select {|i| i.even?})
    odd_vals = array.values_at(* array.each_index.select {|i| i.odd?})

    fft(even_vals)
    fft(odd_vals)

    k = 0
    while k < n / 2
      t = Complex.polar(1.0, -2.0 * Math::PI * k / n) * odd_vals[k]
      array[k] = even_vals[k] + t
      array[k + n / 2] = even_vals[k] - t
      k += 1
    end
    array
  end

  def ifft(array)
    tmp = array.map { |e| e.conj }
    tmp = fft(tmp)
    tmp.map { |e| (e.conj).real / array.size }
  end

  def splice_flatten(volume, chunk)
    out = []
    channel = 0
    while channel < volume.size
      out[channel] = []
      vol = volume[channel].flatten
      until vol.size.zero?
        out[channel] << vol.pop(chunk)
      end
      channel += 1
    end
    out
  end

  def splice_with_stride(volume, chunk, stride)
    stride += chunk
    out = []
    channel = 0
    while channel < volume.size
      out[channel] = []
      i = 0
      while i < volume[channel].size - chunk + 1
        out[channel][i] = []
        j = 0
        while j < volume[channel].size - chunk + 1
          out[channel][i][j] = []
          tmp = []
          k = 0
          while k < chunk
            tmp[k] = []
            l = 0
            while l < chunk
              tmp[k][l] = volume[channel][i + k][j + l]
              l += 1
            end
            k += 1
          end
          out[channel][i][j] = yield tmp
          j += stride
        end
        out[channel][i] -= [nil]
        i += stride
      end
      out[channel] -= [nil]
      channel += 1
    end
    out
  end
end
